import styled from 'styled-components';

export const NavBarModule = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 1rem;
`;

export const Logo = styled.img`
  height: 49px;
`;

export const LanguageList = styled.ul`
  display: flex;
  gap: 1rem;
  place-items: center;
  cursor: pointer;
`;

export const LanguageItem = styled.li`
	&:hover {
	  text-decoration: underline;
	}
`;
