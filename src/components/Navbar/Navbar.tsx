import React from 'react';
import { Logo, NavBarModule, LanguageList, LanguageItem } from './NavbarStyles';
import logo from './../../assets/images/logo.png';

const Navbar = () => {
  const getUrl = (lang: 'es' | 'en') => {
    const currentUrl = window.location.pathname;
    const urlSegments = currentUrl.split('/');
    const urlOtherSegments = urlSegments.slice(2);
    const newUrl = `/${lang}/${urlOtherSegments.join('/')}`;
    return newUrl;
  };

  return (
    <NavBarModule>
      <Logo src={logo} />
      <LanguageList>
        <LanguageItem>
          <a href={`${getUrl('es')}`}>ES</a>
        </LanguageItem>
        <LanguageItem>
          <a href={`${getUrl('en')}`}>EN</a>
        </LanguageItem>
      </LanguageList>
    </NavBarModule>
  );
};

export default Navbar;
