import styled from 'styled-components';

export const Container = styled.div`
  padding: 1rem;
  margin: auto;
  max-width: 500px;
`;

export const Content = styled.div`
  margin-top: 1rem;
`;
