import React from 'react';
import { Navbar } from './../';
import { Container, Content } from './LayoutStyle';
import { PageProps } from 'gatsby';
import { saveLangInLS } from '../../utils/localstorage';
// @ts-ignore
const AppButtons = React.lazy(() => import('@mfButtons/App'));

const Layout: React.FC<PageProps> = ({ children, ...props }) => {
  const { lang } = props.params;
  saveLangInLS(lang);

  return (
    <>
      <Container>
        <Navbar />
        <React.Suspense fallback={<>Cargando</>}>
          <AppButtons />
        </React.Suspense>
        <Content>{children}</Content>
      </Container>
    </>
  );
};

export default Layout;
