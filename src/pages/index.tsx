import React, { useEffect } from 'react';
import { navigate } from 'gatsby';

const IndexPage = () => {
  useEffect(() => {
    navigate('/es/movie/1');
  }, []);

  return null;
};

export default IndexPage;
