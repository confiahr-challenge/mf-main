import React from 'react';
import { PageProps } from 'gatsby';
// @ts-ignore
const AppDetails = React.lazy(() => import('@mfDetails/App'));

const MovieTitle = (props: PageProps) => {
  const { id, lang } = props.params;
  const isSSR = typeof window === 'undefined';

  return (
    <>
      {!isSSR && (
        <React.Suspense fallback={<>Cargando</>}>
          <AppDetails id={id} lang={lang} />
        </React.Suspense>
      )}
    </>
  );
};

export default MovieTitle;
