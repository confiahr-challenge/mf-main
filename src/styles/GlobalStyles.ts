import { createGlobalStyle } from 'styled-components';
import 'reset-css';

export const GlobalStyles = createGlobalStyle`
	body{
      font-family: "Manrope",serif;
	}
	a {
	  text-decoration: none;
	  color: initial;
	}
`;
