import React from 'react';
import Layout from './src/components/layouts/Layout';
import { GlobalStyles } from './src/styles/GlobalStyles';

export const wrapPageElement = ({ element, props }) => {
  return (
    <>
      <GlobalStyles />
      <Layout {...props}>{element}</Layout>
    </>
  );
};
