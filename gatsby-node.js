exports.onCreatePage = ({ page, actions: { createPage } }) =>
  createPage({
    ...page,
    context: { ...page.context, layout: page.path.split('/')[1] || 'index' },
  });
