import type { GatsbyConfig } from 'gatsby';

require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

const config: GatsbyConfig = {
  siteMetadata: {
    title: `main-front`,
    siteUrl: `https://www.yourdomain.tld`,
  },
  // More easily incorporate content into your pages through automatic TypeScript type generation and better GraphQL IntelliSense.
  // If you use VSCode you can also use the GraphQL plugin
  // Learn more at: https://gatsby.dev/graphql-typegen
  graphqlTypegen: true,
  plugins: [
    'gatsby-plugin-styled-components',
    {
      resolve: 'gatsby-plugin-federation',
      options: {
        ssr: false, // Remotes will be fetched during SSG (SSR)
        federationConfig: {
          // B. For your Host
          name: 'appMain',
          remotes: {
            '@mfDetails': process.env.GATSBY_MF_DETAILS,
            '@mfButtons': process.env.GATSBY_MF_BUTTONS,
          },

          shared: {
            'styled-components': {
              singleton: true,
            },
          },
        },
      },
    },
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [
          `Manrope`,
          `Manrope\:200`, // you can also specify font weights and styles
        ],
        display: 'swap',
      },
    },
  ],
};

export default config;
