<p align="center">
  <a href="https://www.gatsbyjs.com/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter-ts">
    <img alt="Gatsby" src="https://www.gatsbyjs.com/Gatsby-Monogram.svg" width="60" />
  </a>
</p>

## 🚀 Instrucciones


1. **Configurar .env.development.**

    ```
    GATSBY_MF_BUTTONS=mfButtons@http://localhost:8002/
    GATSBY_MF_DETAILS=mfDetails@http://localhost:8001/
    ```

2.  **Iniciar MF**

    ```
    npm install
    npm run develop
    ```

    Localhost: http://localhost:8000

    Internet: https://comforting-alfajores-be808f.netlify.app
